import React from 'react'
import { NavLink } from 'react-router-dom'

function HatsList(props) {
    return (
        <React.Fragment>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats.map(hat => {
                        return (
                            <tr key={hat.href}>
                                <td>{hat.style_name}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <button onClick={() => this.delete(hat.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div>
                <NavLink className="nav-link" to="new">Create</NavLink>
            </div>
        </React.Fragment>
    )

}

export default HatsList
