import React, { useState, useEffect } from 'react';

const ShoeForm = (props) => {
    const [ manufacturer, setManufacturer ] = useState("");
    const [ model_name, setModel ] = useState("");
    const [ color, setColor ] = useState("");
    const [ picture_url, setPicture ] = useState("");
    const [ bin, setBin ] = useState();
    const [ bins, setBins ] = useState([]);
    const [ errorMessage, setErrorMessage ] = useState("");

    const clearShoeList = () => {
        setManufacturer("");
        setModel("");
        setColor("");
        setPicture("");
        setBin("");
    }


    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const shoeURL = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                manufacturer: manufacturer,
                model_name: model_name,
                color: color,
                picture_url: picture_url,
                bin: bin
            }),
            headers: {
                "Content-type" : "application/json",
            },
        }
        const response = await fetch(shoeURL, fetchConfig);
        console.log("RESPONSE", response)
        const data = await response.json()
        if (response.ok){
            // const newShoe = await response.json();
            console.log(data)
            clearShoeList()
        } else {
          setErrorMessage(data.message)
        }
    }

    useEffect(() => {
        const getBin = async () => {
            const response = await fetch('http://localhost:8100/api/bins/');
            const data = await response.json();
            setBins(data.bins)
        }
        getBin()
    }, [])

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Shoe</h1>
            <div className="text-danger">{errorMessage}</div>
            <form id="create-shoe-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="manufacturer" required type="text" value={manufacturer} onChange={(event) => setManufacturer(event.target.value)} id="manufacturer" name="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="model_name" required type="text" name="model_name" value={model_name} onChange={(event) => setModel(event.target.value)} id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="color" required type="text" name="color" value={color} onChange={(event) => setColor(event.target.value)} id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="picture_url" required type="url" name="picture_url" value={picture_url} onChange={(event) => setPicture(event.target.value)} id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picutre</label>
              </div>
              <div className="mb-3">
                <select value={bin} onChange={(event => setBin(event.target.value))} required id="bin" name="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map( bin => {
                                    return <option key={bin.bin_number} value={bin.bin_number}>{bin.bin_number}</option>
                                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoeForm
