import {NavLink} from 'react-router-dom'
import React, {useState, useEffect} from "react"


function ShoeList() {

    const [ shoes, setShoes ] = useState([]);

    useEffect(() => {
        const getShoes = async () => {
            const response = await fetch('http://localhost:8080/api/shoes/');
            const data = await response.json();
            console.log(data.shoes)
            setShoes(data.shoes)
        }
        getShoes()
    }, []);


    const deleteShoe = async (id) => {
        const deletionURL = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: 'delete',
        }
        const response = await fetch(deletionURL, fetchConfig)
        if (response.ok) {
            const confirmation = await response.json()
            if (confirmation.deleted === true) {
                setShoes(prevShoes => prevShoes.filter((s) => s.id !== id))
                // same as above
                // setShoes(shoes.filter((s) => s.id !== id))
            }
            console.log("CONFIRMATION", confirmation)
            // window.location.reload(false);
        } else {
            console.log("error::: ",response)
        }
    }

    return(
        <React.Fragment>
        <table  className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture Link</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map((shoes, index) => {
                    return(
                        <tr key={shoes.id}>
                            <td>{shoes.manufacturer}</td>
                            <td>{shoes.model_name}</td>
                            <td>{shoes.color}</td>
                            <td>
                                <a href={shoes.picture_url}>
                                    Click for picture
                                </a>
                            </td>
                            <td>{shoes.bin}</td>
                            <td>
                            <button onClick={() => deleteShoe(shoes.id)} type="button" value="submit" className="deletebtn">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
            <div  className="container px-4 text-center">
                <div  className="row gx-0">
                    <div  className="col">
                    <NavLink  className="btn btn-primary" to="/create" role="button">Create</NavLink>
                    </div>
                </div>
            </div>
    </React.Fragment>
    )
}

export default ShoeList
