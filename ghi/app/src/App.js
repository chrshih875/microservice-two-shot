import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import ShoeList from './ShoeList';

function App(props) {
  if (props.hats === undefined) {
    return null
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/">
          <Route index element={<MainPage />} />
            <Route path="shoes" element={<ShoeList />} />
            <Route path="create" element={<ShoeForm />} />
            <Route path="hats" element={<HatsList hats={props.hats} />} />
            <Route path="hats/new" element={<HatForm/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
