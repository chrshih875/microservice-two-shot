from django.shortcuts import render

from django.views.decorators.http import require_http_methods

from .models import Shoe, BinVO

from common.json import ModelEncoder

from django.http import JsonResponse

import json


class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = [ "import_href",
                "closet_name",
                "bin_number",
                "bin_size"
                ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]

    encoders = {
        "bin": BinVOListEncoder(),
    }

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}


@require_http_methods(["GET", "POST"])
def api_list_shoe(request):
    if request.method == "GET":
        shoe = Shoe.objects.all()
        return JsonResponse({"shoes": shoe}, encoder=ShoeListEncoder, safe=False)
    else:
        content = json.loads(request.body)

        print(content)
        #this is for the bin later
        try:
            bin = BinVO.objects.get(bin_number=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID"},
                status=400
            )
    shoe = Shoe.objects.create(**content)
    print("shoe", shoe)
    return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        print("ID", shoe)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0 })
    else:
        content =json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(bin_number=content["bin_number"])
                content["bin_number"] = bin
        except BinVODoesNotExist:
            return JsonResponse({"message": "Invalid Shoe ID"},
                status=400,)

    Shoe.objects.filter(id=pk).update(**content)
    shoe = Shoe.objects.get(id=pk)
    return JsonResponse(
        shoe, encoder=ShoeDetailEncoder, safe=False
    )
