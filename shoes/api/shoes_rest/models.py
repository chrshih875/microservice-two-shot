from django.db import models

# Create your models here.
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(max_length=500)
    bin = models.ForeignKey("BinVO", related_name="shoe", on_delete=models.CASCADE)

    def __str__(self):
        return self.model_name



class BinVO(models.Model):
    import_href = models.CharField(max_length=50, unique=True)
    closet_name = models.CharField(max_length=50)
    bin_number = models.PositiveSmallIntegerField(unique=True)
    bin_size = models.PositiveSmallIntegerField()


    def __str__(self):
        return self.closet_name

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")
