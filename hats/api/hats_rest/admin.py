from django.contrib import admin

# Register your models here.
from .models import Hat, LocationVO


admin.site.register(Hat)

admin.site.register(LocationVO)
